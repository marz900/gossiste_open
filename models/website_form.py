# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools, _
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval

import logging
_logger = logging.getLogger(__name__)

class GrossisteOpenWebsite(models.Model):

    _inherit = "website"

    def get_grossiste_details(self):
        partner_brw = self.env['res.users'].browse(self._uid)
        grossiste_ids = self.env['form.grossiste'].sudo().search(['|',('partner_id','=',partner_brw.partner_id.id),('user_id','=',partner_brw.id)])
        return grossiste_ids

    # def get_grossiste_type(self):
    #     ticket_type_ids = self.env['support.ticket.type'].sudo().search([])
    #     return ticket_type_ids

    # def get_team_details(self):
    #     support_team = self.env['support.team'].sudo().search([])
    #     team_list = []
    #     for team in support_team :
    #         team_list.append(team)
    #     return team_list

    def get_partner_details(self):
        partner_id = self.env['res.partner'].sudo().search([])
        partner_list = []
        for partner in partner_id :
            partner_list.append(partner)
        return partner_list
    
    def get_partner_details(self):
        partner_id = self.env['res.partner'].sudo().search([])
        _partner = []
        for partner in partner_id :
            _partner.append(partner)
        return _partner

    # def get_category_details(self):
    #     ticket_type_ids = self.env['support.ticket.type'].sudo().search([])
    #     category_list = []
    #     for category in ticket_type_ids :
    #         category_list.append(category)
    #     return category_list

    # def get_project_details(self):
    #     project_id = self.env['project.project'].sudo().search([('privacy_visibility','=','portal'),('allowed_portal_user_ids', 'in', [self.env.user.id])])
    #     project_list = []
    #     for project in project_id :
    #         project_list.append(project)
    #     return project_list

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
