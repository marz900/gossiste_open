from email.policy import default
from importlib.metadata import requires
import string
from odoo import models, fields, api, _
import logging
from datetime import timedelta

logger = logging.getLogger(__name__)

class FormGrossite(models.Model):
    _name = 'form.grossiste'
    
    
    
    name = fields.Many2one('res.users', string="E-mail commercial", requires=True)
    name_manager = fields.Char(string="Nom du Responsable", requires=True)
    gender = fields.Selection([('m', 'Homme'), ('f', 'Femme')], default=fields, string="Genre")
    work = fields.Selection([('d', 'Distributeur'), ('g', 'Grossiste'), ('v', 'Revendeur'), ('a', 'Autre')], default=fields, required=True, string="Catégorie")
    address = fields.Text(string="Adresse", required=True)
    phone = fields.Integer(string="Téléphone", required=True)
    nameBAbidjan = fields.Char(string="Nom des boutiques abidjan")
    nameBItern = fields.Char(string="Nom des boutiques a l’intérieur")
    nb_boutique = fields.Integer(string="Nombre de Boutique")
    nb_vendu = fields.Integer(string="Nombre de pièces vendus par mois")
    city = fields.Char(string="Ville", required=True)
    contact_manager = fields.Char(string="E-mail du responsable")
    card = fields.Char(string="CNI")
    images = fields.Image(string="Photo de la Boutique", max_width=100, max_height=100, verify_resolution=False)
    card_image = fields.Image(string="Photo de la Carte d\'identité recto", max_width=100, max_height=100, verify_resolution=False)
    card_image1 = fields.Image(string="Photo de la Carte d\'identité verso", max_width=100, max_height=100, verify_resolution=False)
    card_image_sign = fields.Image(string="Photo de la signature", max_width=100, max_height=100, verify_resolution=False)
    choice_about = fields.Boolean(string="Oui/Non")
    
    
    
    