# -*- coding: utf-8 -*-
import werkzeug
import json
import base64

import odoo.http as http
from odoo.http import request
from odoo import SUPERUSER_ID
from datetime import datetime, timedelta, time
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager
import odoo.http as http

class GossisteOpen(http.Controller):
    @http.route('/grossiste/form/', type="http", auth="public", website=True)
    def create_grossiste(self, **kw):
        request.env['form.grossiste'].sudo().create(kw)
        return request.render("grossiste_open.grossiste_thank_you", {})

    # @http.route('/gossiste_open/gossiste_open/objects/', auth='public')
    # def list(self, **kw):
    #     return http.request.render('gossiste_open.listing', {
    #         'root': '/gossiste_open/gossiste_open',
    #         'objects': http.request.env['gossiste_open.gossiste_open'].search([]),
    #     })

    # @http.route('/gossiste_open/gossiste_open/objects/<model("gossiste_open.gossiste_open"):obj>/', auth='public')
    # def object(self, obj, **kw):
    #     return http.request.render('gossiste_open.object', {
    #         'object': obj
    #     })
        
# class GrossisteForm(CustomerPortal):

#     @http.route('/grossiste_form', type="http", auth="public", website=True)
#     def submit_grossiste_form(self, **kw):
#         """Let's public and registered user submit a support ticket"""
#         name = ""
#         if http.request.env.user.name != "Public user":
#             name = http.request.env.user.name
        
#         customer = http.request.env.user.partner_id.name
#         email = http.request.env.user.partner_id.email
#         phone = http.request.env.user.partner_id.phone
#         values = {'partner_id' : customer,'user_ids': name,'email':email,'phone':phone}
        
#         return http.request.render('grossiste_form.submit_grossiste_form', values)
    
    
#     @http.route('/grossiste_form/thanks', type="http", auth="public", website=True)
#     def support_grossiste_thanks(self, **post):
#         """Displays a thank you page after the user submits a support ticket"""
#         if post.get('debug'):
#             return request.render("grossiste_form.support_thank_you")